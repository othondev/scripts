#!/bin/bash

getStatus() {
  out=$( curl --silent $1 )
  if echo $out | jq -e . >> /dev/null 2>&1; then
    code=$( curl --silent $1 | jq '. | "\(.project) = v\(.version)"' | sed -r 's/"//g' )
    echo $code
  else
    echo $out
  fi
}


echo "SERVICES STATUS"

echo "${BLUE}PIE-STACK:${NC}"
echo $(getStatus "https://test.softfax.api.hp.com/api/v2/healthcheck" )
echo $(getStatus "https://sws.pie.hp8.us/sws/api/v1/healthcheck" )

echo DEV-STACK:
echo $(getStatus "https://dev.softfax.api.hp.com/api/v2/healthcheck" )
echo $(getStatus "https://sws.dev.hp8.us/sws/api/v1/healthcheck" )

echo STAGE-STACK:
echo $(getStatus "https://stage.softfax.api.hp.com/api/v2/healthcheck" )
echo $(getStatus "https://sws.stage.hp8.us/sws/api/v1/healthcheck" )

echo PROD-STACK:
echo $(getStatus  "https://softfax.api.hp.com/api/v2/healthcheck" )
echo $(getStatus  "https://sws.hp8.us/sws/api/v1/healthcheck" )